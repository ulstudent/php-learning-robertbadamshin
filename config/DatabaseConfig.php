<?php

return [
    'host'=>'localhost',  //not necessarily
    'dbname'=>'test',     //necessarily
    'charset'=>'utf8',   //necessarily
    'username' => 'root',  //necessarily
    'password' => '',    //necessarily
    'options' => array(  //not necessarily
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    )
];
