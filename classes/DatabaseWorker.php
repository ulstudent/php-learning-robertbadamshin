<?php

include_once 'Exception.php';

/**
 * Class DatabaseWorker
 *
 *  Класс для работы с бзой данных.
 *
 */
 class DatabaseWorker
 {
     /**
      * @var sql запрос
      */
     private $query;
     /**
      * @var путь к файлу с настройками подключения к базе данных
      */
     private $pathToConfig = "../config/DatabaseConfig.php";
     /**
      * @var array список sql - инъекций
      */
     private $injections = ['create ','insert ','update ','delete ',
            'join ',' select ','drop ',' -- ',' or ' ,' from ' ,
            ' group by ', '"' , "'" ,  '%' , '_'] ;
     /**
      * @var часть запроса с ключевым словом TABLE
      */
     private $table;
     /**
      * @var часть запроса с ключевым словом WHERE
      */
     private $where;
     /**
      * @var хранит поля в которые вставляются данные в insert-запросах
      */
     private $columns;
     /**
      * @var хранит данные в insert-запросах
      */
     private $values;
     /**
      * @var хранит параметры для bindParam
      */
     private $prepareParameters;

     /**
      * @var связь с базой данных
      */
     public $dblink;

     /** Констурктор
      *
      *  Берет настройки из файла databaseConfig и подключается к базе данных
      *
      * @throws ErrorConnectToDatabaseException
      * @throws FileNotExistsException
      * @throws IncorrectDatabaseConfigException
      */
     public function  __construct()
     {
         if (!@file_exists($this->pathToConfig)) {
             throw new FileNotExistsException($this->pathToConfig);
         }

         $config = include "$this->pathToConfig";

         foreach (['dbname', 'charset', 'username', 'password'] as $parameter)
         {
             if (!isset($config[$parameter]))
             {
                 throw new IncorrectDatabaseConfigException($parameter);
             }
         }

        if(!$this->dblink = new \PDO("mysql:host=".$config['host'].";dbname=".$config['dbname'].";charset=".$config['charset'],
                                $config['username'],
                                $config['password'],
                                $config['options']))
            throw new ErrorConnectToDatabaseException();
    }

     /** Проверка правильности операторап сравнения
      *
      * @param $sign
      * @throws NotAssignValueToTypeException
      */
     private function CheckCompareOperator($sign)
     {

         if (preg_match("/^(<|>|=|<=|>=|!=|<>|<==>)$/",$sign ) == false )//(preg_match_all("/ ^[<]$ | ^[>]$ |^[=]$  | ^[<=]$  | ^[>=]$  | ^[!=]$  | ^[<>]$  | ^[<==>]$ /",$sign ) == false )//($sign == "<" or $sign == ">" or $sign == "=" or $sign == "<=" or $sign == ">=" or $sign == "!=" or $sign == "<>" or $sign == "<==>")
         {
             throw new NotAssignValueToTypeException($sign);

         }
         else
         {
             return;
         }
     }

     /** Чистка параметров
      *
      * Обнуляет переменные класса, требуется после выполнения запросов
      */
     private function CleanParameters()
     {
         $this->table = null;
         $this->where = null;
         $this->columns = null;
         $this->values = null;
         $this->prepareParameters = null;
         $this->query = null;
     }

     /** select
      *
      *  Используется в первой при созданий select-запроса.
      *
      * @param $rows - столбцы таблицы которые нужно обработать
      * @return $this
      * @throws InjectionInDataException
      */
     public function select($rows)
     {
         $this->query = "";
         $this->query = "SELECT ";
         foreach($rows as $row)
         {
             $this->query .= $row.", ";
         }
         $this->query[strripos($this->query,",")]=" "; //убирает последнюю запятую
         return $this;
     }

     /** Таблица
      *
      *  Используется при построении select-запроса после функции select()
      *
      * @param $table - название таблицы
      * @return $this
      * @throws InjectionInDataException
      */
     public function from($table)
     {
         $this->query .= " FROM ".$table;
         return $this;
     }

     /** Where
      *
      * Используется для построения запроса с ключевым словом where.
      * Присваивает значение и полю this->where и прибавляет к полю this->query
      *
      * @param $row - название столбца
      * @param $sign - операор сравнения
      * @param $value - значение (строка или число)
      * @return $this
      * @throws InjectionInDataException
      * @throws NotAssignValueToTypeException
      */
     public function where($row,$sign,$value)
     {
         $this->CheckCompareOperator($sign);

         $this->prepareParameters['value'] = $value;

         $this->query .= " WHERE " . $row . $sign . ":value" ;  //для использования в запросах select

         $this->where =  " WHERE " . $row . $sign .  ":value" ;

         return $this;
     }

     /** Limit
      *
      *  Используется для построения select-запросов с ключевым словом limit.
      *
      * @param $number1 - первый параметр
      * @param null $number2 - второй параметр (по умолчанию null)
      * @return $this
      * @throws InjectionInDataException
      */
     public function limit($number1 ,$number2 = null)
     {
         if ($number2 == null)
         {
             $this->prepareParameters['start'] = $number1;
             $this->query .= " LIMIT :start";
         }
         else
         {
             $this->prepareParameters['start'] = $number1;
             $this->prepareParameters['finish'] = $number2;
             $this->query .= " LIMIT :start, :finish ";
         }
         return $this;
     }

     /** Offset
      *
      * Функция для обработки offset
      *
      * @param $offset
      * @return $this
      */
     public function offset($offset)
     {
         $this->prepareParameters['offset'] = $offset;
         $this->query .= " OFFSET :offset";

         return $this;
     }

     /** Execute
      *
      * Выполняет подстановку подготовленных параметров запроса из this->prepareParameters и выполняет select-запрос.
      *
      * @return PDOStatement
      * @throws ErrorExecuteException
      */
     public function execute()    //делает запрос к бд и возвращает результат
     {
         echo $this->query;
         $prepareStatement = $this->dblink->prepare($this->query);

         if(strripos($this->query,':value')!==false)
         {
             $prepareStatement->bindValue(":value",$this->prepareParameters['value'],PDO::PARAM_INT);
         }
         if(strripos($this->query,':start')!==false)
         {
             $prepareStatement->bindValue(":start",$this->prepareParameters['start'],PDO::PARAM_INT);
         }
         if(strripos($this->query,':finish')!==false)
         {
             $prepareStatement->bindValue("finish",$this->prepareParameters['finish'],PDO::PARAM_INT);
         }
         if(strripos($this->query,':offset')!==false)
         {
             $prepareStatement->bindValue("offset",$this->prepareParameters['offset'],PDO::PARAM_INT);
         }

         $this->CleanParameters();

         if($prepareStatement->execute())
         {
             return $prepareStatement ;
         }
         else
             throw new ErrorExecuteException($this->query);
     }

     /** Table
      *
      * Заполняет поле table именем таблицы, к которой выполняется запрос
      *
      * @param $table
      * @return $this
      * @throws InjectionInDataException
      */
     public function table($table)
     {
         $this->table = $table;
         return $this;
     }

     /** Columns
      *
      * В insert-запросах заполняет переменную this->columns массивом столбцов в которые вставляются данные
      *
      * @param $rows - массив Стобцов
      * @return $this
      * @throws InjectionInDataException
      */
     public function columns($rows)
     {
         $this->columns = "(";

         foreach ($rows as $row)
         {
             $this->columns .= $row . ", ";
         }
         $this->columns[strripos($this->columns,",")]=" ";
         $this->columns .= ')';

         return $this;
     }

     /** Values
      *
      * В insert-запросах заполняет переменную this->values массивом значений
      *
      * @param $rows - массив значений
      * @return $this
      * @throws InjectionInDataException
      */
     public function values($rows)
     {
         $this->values = " (";

         foreach ($rows as $row)
         {
             $this->values .= "'$row'" . ", ";
             $this->prepareParameters[] = $row;
         }
         $this->values[strripos($this->values,",")]=" ";
         $this->values .= ')';

         return $this;
     }

     /** Insert
      *
      * Подготоавливает и выполняет insert запрос
      *
      * @return bool - в случае успешного выполнения
      * @throws ErrorExecuteException
      */
     public function insert()
     {
         $prepareInsertValuesString = " ( ";
         for($i = 0; $i < count($this->prepareParameters);$i++)
         {
             $prepareInsertValuesString .= "?, ";
         }
         $prepareInsertValuesString = substr($prepareInsertValuesString,0,-2);
         $prepareInsertValuesString .= " ) ";

         $query = "INSERT INTO " . $this->table . $this->columns . " VALUES " . $prepareInsertValuesString;
         $prepareStatement = $this->dblink->prepare($query);
         for($i = 0; $i < count($this->prepareParameters);$i++)
         {
             $prepareStatement->bindParam($i+1,$this->prepareParameters[$i]);
         }

         $this->CleanParameters();

         if($prepareStatement->execute())
         {
             return true;
         }
         else
             throw new ErrorExecuteException($query);
     }

     /** Delete
      *
      * Подготавливает и выполняет delete-запрос
      *
      * @return bool
      * @throws ErrorExecuteException
      */
     public function delete()
     {
         $query = "DELETE FROM " . $this->table . $this->where;

         $prepareStatement = $this->dblink->prepare($query);
         if(strripos($query,':value') !== false)
             $prepareStatement->bindParam(':value',$this->prepareParameters['value']);

         $this->CleanParameters();

         if($prepareStatement->execute())
         {
             return true;
         }
         else
             throw new ErrorExecuteException($query);
     }

     /** Update
      *
      * Подготавливает и выполняет Update запрос
      *
      * @param $arrayOfPair
      * @return bool
      * @throws ErrorExecuteException
      * @throws InjectionInDataException
      */
     public function update($arrayOfPair)
     {
         $query = " UPDATE " . $this->table . " SET ";

         foreach($arrayOfPair as $pair)
         {
             $query .= " $pair[0] = :$pair[1] ," ;
             $this->prepareParameters[$pair[1]] = $pair[1];
         }
         $query[strripos($query,",")]=" ";
         $query .= $this->where;

         $prepareStatement = $this->dblink->prepare($query);
         foreach($this->prepareParameters as $key=>&$value)
         {
             $prepareStatement->bindParam(":$key",$value);
         }

         $this->CleanParameters();

         if($prepareStatement->execute())
         {
             return true;
         }
         else
             throw new ErrorExecuteException($query);
     }

     /** ExecuteQuery
      *
      * Функция для выполнения sql-запроса вручную.
      *
      * @param $query
      * @return PDOStatement
      */
     public function executeQuery($query)
     {
         return $prepareQuery = $this->dblink->query($query);
     }

     /** Join
      *
      * Подготавливает и выполняет Join-запрос. Выполняется после table.
      * Внутреннее (inner) объединение
      *
      * @param $secondTable - вторая таблица с которой объединяются данные
      * @param $secondTableField - поле второй таблицы для сравнения
      * @param $sign - оператор сравнения
      * @param $firstTableField - поле первой таблицы для сравнения
      * @return PDOStatement - результат запроса
      * @throws InjectionInDataException
      * @throws NotAssignValueToTypeException
      */
     public function innerJoin($secondTable,$secondTableField,$sign,$firstTableField)
     {
         $this->CheckCompareOperator($sign);
         $query = " SELECT * FROM " . $this->table . "," . $secondTable . " WHERE " . $firstTableField . $sign . $secondTableField ;

         $this->CleanParameters();

         return $this->dblink->query($query);
     }

     /**leftJoin
      *
      * Выполняет leftJoin(левостороннее внешнее объединение)-запрос
      *
      * @param $firstTable
      * @param $secondTable
      * @param $using
      * @return PDOStatement
      */
     public function leftJoin($firstTable,$secondTable,$using)
     {
         $query = " SELECT * FROM " . $firstTable . " LEFT JOIN " . $secondTable . " USING(".$using.")" ;

          return  $this->dblink->query($query);

     }

     /** rightJoin
      *
      * Выполняет rightJoin(правостороннее внешнее объединение)-запрос
      *
      * @param $firstTable
      * @param $secondTable
      * @param $using
      * @return PDOStatement
      */
     public function rightJoin($firstTable,$secondTable,$using)
     {
         $query = " SELECT * FROM " . $firstTable . " RIGHT JOIN " . $secondTable . " USING(".$using.")" ;

         return  $this->dblink->query($query);
     }



 }