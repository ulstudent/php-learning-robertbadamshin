<?php

class IncorrectDatabaseConfigException extends Exception
{
    public function __construct($param)
    {
        $this->message = "$param must be defined in config/db.php ;";
    }
}

class InjectionInDataException extends Exception
{
    public function __construct($param)
    {
        $this->message = "$param contains database injection.";
    }
}

class FileNotExistsException extends Exception
{
    public function __construct($param)
    {
        $this->message = "$param file does not exists.";
    }
}

class ErrorConnectToDatabaseException extends Exception
{
    public function __construct()
    {
        $this->message = "Could not connect to database;";
    }
}

class ErrorExecuteException extends Exception
{
    public function __construct($query)
    {
        $this->message = "Error in execute this query: $query";
    }
}

class NotAssignValueToTypeException extends Exception
{
    public function __construct($value)
    {
        $this->message = "Value $value does not assign to its type;";
    }
}



