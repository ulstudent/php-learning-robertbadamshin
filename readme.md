**Библиотека для работы с базой данных**

Для начала работы необходимо создать экземпляр класса databaseWorker ,передав в конструктор настройки подключения.
Эта библиотека может поддерживать любую систему управления базами данных, для которой существует PDO-драйвер.

**Пример выполнения select-запроса:**

$db->select(['Id','Name'])
    ->from('TestTable')
    ->where('Id','<',4)
    ->limit(1)
    ->offset(2)
    ->execute()
    ->fetchAll();

**Пример выполнения insert-запроса:**

$db->table('TestTable')
	->columns(['Id','Name','Text'])
	->values([8,Name5,Text5])
	->insert();

**Пример выполнения delete-запроса:**

$db->table("TestTable")
	->where('Id','>',7)
	->delete();

**Пример выполнения update-запроса:**

$db->table("TestTable")
	->where('Id','<=',1)
	->update([["Name","Name1"],['Text','Text1']]);

**Пример выполнения sql-запроса вручную:**

$db->executeQuery("select * from TestTable")
	->fetchAll();

**Пример выполнения innerJoin-запроса:**

$db->table("TestTable")
	->join('TestTable2','TestTable2.Id','=','TestTable.Id')
	->fetchAll();
	
**Пример выполнения leftJoin-запроса:**

var_dump($db->leftJoin("TestTable","TestTable2","Id")
	->fetchAll());
	
**Пример выполнения rightJoin-запроса:**

var_dump($db->rightJoin("TestTable","TestTable2","Id")
	->fetchAll());